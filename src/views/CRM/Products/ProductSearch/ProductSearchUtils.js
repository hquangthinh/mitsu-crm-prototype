
const range = len => {
  const arr = [];
  for (let i = 0; i < len; i++) {
    arr.push(i);
  }
  return arr;
};

const newProduct = () => { 
  return {
    productName: 'Tesla model x',
    productSerial: 'xxxxyyyyzzzz',
    numberInStock: Math.floor(Math.random() * 100),
    unitCost: 20000
  };
};

export function makeData(len = 5553) {
  return range(len).map(d => {
    return {
      ...newProduct()
    };
  });
}
