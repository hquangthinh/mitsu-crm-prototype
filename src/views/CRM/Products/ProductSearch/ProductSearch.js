import React, {Component} from 'react';
import {
  Badge,
  Row,
  Col,
  Card,
  CardHeader,
  CardBody,
  Table,
  Pagination,
  PaginationItem,
  PaginationLink,
  InputGroup,
  Input
} from 'reactstrap';
import ReactTable from "react-table";
import "react-table/react-table.css";
import { makeData } from './ProductSearchUtils';

class ProductSearch extends Component {

  constructor() {
    super();
    this.state = {
      data: makeData()
    };
  }

  render() {
    
    const { data } = this.state;    

    return (
      <div className="animated fadeIn">
        <Row>
          <Col xs="12" lg="12">
            <Card>
              <CardHeader>
                <i className="fa fa-align-justify"></i> Products
              </CardHeader>
              <CardBody>
                <ReactTable
                  data={data}
                  filterable
                  defaultFilterMethod={(filter, row) =>
                    String(row[filter.id]) === filter.value}
                  columns={[
                    {
                      Header: "Tên xe",
                      accessor: "productName",
                      filterMethod: (filter, row) =>
                        row[filter.id].startsWith(filter.value) &&
                        row[filter.id].endsWith(filter.value)
                    },
                    {
                      Header: "Số khung",
                      id: "productSerial",
                      accessor: d => d.productSerial,
                      filterMethod: (filter, rows) =>
                        matchSorter(rows, filter.value, { keys: ["productSerial"] }),
                      filterAll: true
                    },                    
                    {
                      Header: "SL tồn kho",
                      accessor: "numberInStock"
                    },
                    {
                      Header: "Đơn giá",
                      accessor: "unitCost",
                      id: "unitCost",
                      filterMethod: (filter, row) => {
                        if (filter.value === "all") {
                          return true;
                        }
                        if (filter.value === "true") {
                          return row[filter.id] >= 21;
                        }
                        return row[filter.id] < 21;
                      }
                    }
                  ]}
                  defaultPageSize={10}
                  className="-striped -highlight"
                />                                                
              </CardBody>
            </Card>
          </Col>
        </Row>                
      </div>

    )
  }
}

export default ProductSearch;
